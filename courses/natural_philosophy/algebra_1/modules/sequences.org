#+TITLE:     Module 1: Sequences
#+AUTHOR:    Thomas Trabue
#+EMAIL:     tom.trabue@gmail.com
#+DATE:      2024-03-28
#+TAGS:      algebra one module
#+SETUPFILE: ~/.emacs.d/setupfile.org
#+OPTIONS:   date:nil toc:nil author:nil title:t num:nil
#+STARTUP:   fold
#+LATEX_HEADER: \theoremstyle{reddef}
#+LATEX_HEADER: \newtheorem{definition}{Definition}
#+LATEX_HEADER: \theoremstyle{bluethm}
#+LATEX_HEADER: \newtheorem{theorem}{Theorem}
#+LATEX_HEADER: \theoremstyle{greenex}
#+LATEX_HEADER: \newtheorem{exercise}{Exercise}

* Introduction

** What is a Sequence?
# Blanks: list, in, order, commas
\begin{doublespace}
\begin{definition}[Sequence]
A sequence is a \rule{1.5cm}{0.15mm} of things that are \rule{0.75cm}{0.15mm}
\rule{2cm}{0.15mm}. The things in the sequence are usually separated by
\rule{2cm}{0.15mm}.
\end{definition}
\end{doublespace}

# Blanks: finite, infinite
Sequences can be \rule{2cm}{0.15mm} or \rule{2.5cm}{0.15mm}.

** Finite Sequences

*** Examples

\(1, 2, 3\)

\(a, b, c\)

\begin{exercise}
Come up with \textbf{three} examples of sequences, and list them in the space below.
\end{exercise}

\begin{enumerate}
\item{}
\item{}
\item{}
\end{enumerate}

** Implied Elements

*** Examples

\(1, 2, 3, \ldots\)

\(a, b, c, \ldots, z\)
