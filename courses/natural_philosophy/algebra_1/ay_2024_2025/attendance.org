#+SETUPFILE: ~/.emacs.d/setupfile.org
#+LATEX_HEADER: \usepackage{nopageno}
#+LATEX_HEADER: \geometry{letterpaper, portrait, margin=0.5in}
#+TITLE:   Algebra 1 Attendance
#+AUTHOR:  Tom Trabue
#+EMAIL:   tom.trabue@gmail.com
#+DATE:    2024-08-20
#+TAGS:
#+OPTIONS: date:nil toc:nil author:nil title:nil num:nil

\doublespace

* Algebra 1 \hfill Date: \rule{4cm}{0.5pt}

\textbf{H} Michael Bender: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Gabi Biancheri: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Ella Foreman: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Cooper Hancock: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Kyle Harlow: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Bella Hatfield: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Clara Hillard: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Peter Jebasingh: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Chloe Jones: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Richie Lupescu: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Fynn McCoy: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Jude McNeil: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Eva Newton: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Noelle Pacula: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Micah Parks: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

\textbf{H} Rylee Poplin: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}

Sophia Swann: \hrulefill{}
Grade 1: \rule{1.5cm}{0.5pt}
Grade 2: \rule{1.5cm}{0.5pt}
Grade 3: \rule{1.5cm}{0.5pt}
CP: \rule{1.5cm}{0.5pt}
