# Franklin Classical School Documents

Repository for my FCS course documents and administrative files.

## File Organization

I organize my course files by academic year, which allows me to keep multiple
file versions available at once. Of course, Git helps retrieve old versions of
these files based on this repostitory's commit history, but I often want to see
which version of a file was "current" when I last printed it in a given school
year, and my file system does a fine job accomplishing this task.
